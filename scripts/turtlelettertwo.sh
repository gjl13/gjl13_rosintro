# Move to G Start
rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 3 10 3.14
rosservice call /turtle1/set_pen 255 0 255 20 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[10.0, 0.0, 0.0]' ' [0.0, 0.0, 4.68]'
rosservice call /turtle1/teleport_relative 0 1.57
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.0, 0.0, 0.0]' ' [0.0, 0.0, 0.0]'

# Move to J Start
rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 3 7 0
rosservice call /turtle1/set_pen 0 255 255 10 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' ' [0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-2.0, 0.0, 0.0]' ' [0.0, 0.0, 0.0]'
rosservice call /turtle1/teleport_relative 0 4.715
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3.5, 0.0, 0.0]' ' [0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3.5, 0.0, 0.0]' ' [0.0, 0.0, -3.2]'

# Move to L Start
rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 6 8.5 4.715
rosservice call /turtle1/set_pen 255 255 0 15 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' ' [0.0, 0.0, 0.0]'
rosservice call /turtle1/teleport_relative 0 1.56
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.5, 0.0, 0.0]' ' [0.0, 0.0, 0.0]'

# Relocate turtle
rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 10 1 1.55
rosservice call /turtle1/set_pen 255 255 0 15 0
